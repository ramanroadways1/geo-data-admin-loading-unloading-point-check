<?php
include("_header_datatable.php");
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Branch Wise Address - Book : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT user.username as branch,
  coalesce((SELECT COUNT(address_book_consignor.id)
   FROM address_book_consignor
   WHERE address_book_consignor.branch =  user.username GROUP BY address_book_consignor.branch
  ),0) AS total_con1,
  
  coalesce((SELECT COUNT(address_book_consignor.id)
   FROM address_book_consignor
   WHERE address_book_consignor.branch =  user.username
    AND date(address_book_consignor.timestamp)='".date("Y-m-d")."' GROUP BY address_book_consignor.branch
  ),0) AS total_con1_today,
  
  coalesce((SELECT COUNT(address_book_consignee.id)
   FROM address_book_consignee
   WHERE address_book_consignee.branch =  user.username GROUP BY address_book_consignee.branch
  ),0) AS total_con2,
  
  coalesce((SELECT COUNT(address_book_consignee.id)
   FROM address_book_consignee
   WHERE address_book_consignee.branch =  user.username
   AND date(address_book_consignee.timestamp)='".date("Y-m-d")."' GROUP BY address_book_consignee.branch
  ),0) AS total_con2_today
  
FROM user
WHERE user.role='2' AND user.username NOT IN('HEAD','DUMMY')
GROUP BY user.username
ORDER BY user.username ASC");

if(!$qry){
	echo mysqli_error($conn);
}
?>			  
	
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Branch</th>
                        <th>Total Loading Points</th>
                        <th>Added Today</th>
						<th>Total Unloading Points</th>
                        <th>Added Today</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='5'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			echo "<tr>
				<td>$i</td>
				<td>$row[branch]</td>
				<td>$row[total_con1]</td>
				<td>$row[total_con1_today]</td>
				<td>$row[total_con2]</td>
				<td>$row[total_con2_today]</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  