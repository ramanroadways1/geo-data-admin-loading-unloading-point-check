<?php
$ThisPage = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Raman Roadways Pvt Ltd : Raman Group.</title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="../admin_lte/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="../admin_lte/dist/css/AdminLTE.min.css">
		<link rel="stylesheet" href="../admin_lte/dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="../admin_lte/plugins/iCheck/flat/blue.css">
		<link rel="stylesheet" href="../admin_lte/plugins/morris/morris.css">
		<link rel="stylesheet" href="../admin_lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
		<link rel="stylesheet" href="../admin_lte/plugins/datepicker/datepicker3.css">
		<link rel="stylesheet" href="../admin_lte/plugins/daterangepicker/daterangepicker-bs3.css">
		<link rel="stylesheet" href="../admin_lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
		
		<script src="../diary/sweet_alert_lib/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="../diary/sweet_alert_lib/dist/sweetalert2.min.css">
		<script src="https://kit.fontawesome.com/d5ec20b4be.js" crossorigin="anonymous"></script>
	</head>
	
<?php
include("_menu.php");
?>