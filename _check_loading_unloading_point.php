<?php
require_once './_connect.php'; 

$elem = escapeString($conn,($_POST['elem']));
$location = escapeString($conn,($_POST['location']));
$party_id = escapeString($conn,($_POST['party_id']));
$type = escapeString($conn,($_POST['type']));

echo "<script>
	$('#own_tno').val('');
	$('#visit_date').val('');
	$('#loading_point').val('');
	$('#search_loading_point').val('');
	$('#loading_pincode').val('');
	$('#cord_lat').val('');
	$('#cord_long').val('');
	
	$('.cord_div').hide();
	$('.google_div').hide();
	$('.own_truck_div').show();
</script>";

if($location=='' || $party_id=='')
{
	AlertErrorTopRight("Enter location and party first !");
	exit();
}

if($type=='loading')
{
	$chk = Qry($conn,"SELECT id FROM address_book_consignor WHERE from_id='$location' AND consignor='$party_id'");
	
	if(!$chk){
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
}
else if($type=='unloading')
{
	$chk = Qry($conn,"SELECT id FROM address_book_consignee WHERE to_id='$location' AND consignee='$party_id'");
	
	if(!$chk){
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}
}
else
{
	AlertErrorTopRight("Invalid selection !");
	exit();
}

if(numRows($chk) >0 )
{
	AlertErrorTopRight("Duplicate record found !");
	echo "<script>
		$('#way_to_add').val('');
	</script>";
	exit();
}

if($elem=='Own_Truck')
{
	echo "<script>
		$('.google_div').hide();
		$('.cord_div').hide();
		$('.own_truck_div').show();
		
		$('#search_loading_point').attr('required',false);
		$('#cord_lat').attr('required',false);
		$('#cord_long').attr('required',false);
									
		$('#own_tno').attr('required',true);
		$('#visit_date').attr('required',true);
		$('#loading_point').attr('required',true);
	</script>";
}
else if($elem=='Google')
{
	echo "<script>
		$('.google_div').show();
		$('.cord_div').hide();
		$('.own_truck_div').hide();
		
		$('#search_loading_point').attr('required',true);
		$('#cord_lat').attr('required',false);
		$('#cord_long').attr('required',false);
									
		$('#own_tno').attr('required',false);
		$('#visit_date').attr('required',false);
		$('#loading_point').attr('required',false);
	</script>";
}
else if($elem=='Coordinates')
{
	echo "<script>
		$('.google_div').hide();
		$('.cord_div').show();
		$('.own_truck_div').hide();
		
		$('#search_loading_point').attr('required',false);
		$('#cord_lat').attr('required',true);
		$('#cord_long').attr('required',true);
									
		$('#own_tno').attr('required',false);
		$('#visit_date').attr('required',false);
		$('#loading_point').attr('required',false);
	</script>";
}
else
{
	echo "<script>
		$('.google_div').hide();
		$('.Coordinates').hide();
		$('.own_truck_div').show();
		
		$('#search_loading_point').attr('required',false);
		$('#cord_lat').attr('required',false);
		$('#cord_long').attr('required',false);
									
		$('#own_tno').attr('required',true);
		$('#visit_date').attr('required',true);
		$('#loading_point').attr('required',true);
	</script>";
}

echo "<script>
		$('#location').attr('readonly',true);
		$('#party_name').attr('readonly',true);
		$('#loadicon').fadeOut('slow');
	</script>";
exit();
?>