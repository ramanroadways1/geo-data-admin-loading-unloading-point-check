<?php
include("_header.php");
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> 

<script type="text/javascript">
$(document).ready(function (e) {
$("#Form1").on('submit',(function(e) {
// $("#loadicon").show();
$("#submit_icon").hide();
$("#spinner_icon").show();
// $("#button_sub").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_new_point.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>
	
<script>
$(function() {
		$("#location").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#location').val(ui.item.value);   
            $('#to_id').val(ui.item.id);      
            $('#pincode').val(ui.item.pincode);      
            $('#state').val(ui.item.state);      
            $('#loc_lat_long').val(ui.item.lat_lng);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#location').val("");   
			$('#to_id').val("");   
			$('#pincode').val("");   
			$('#state').val("");   
			$('#loc_lat_long').val("");   
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Location does not exists.</font>',});
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {
		$("#own_tno").autocomplete({
		source: '../diary/autofill/get_own_vehicle.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#own_tno').val(ui.item.value);   
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#own_tno').val("");   
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Vehicle does not exists.</font>',});
			// alert('Vehicle does not exists.');
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {  
      $("#party_name").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: '../b5aY6EZzK52NA8F/autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#party_name').val(ui.item.value);   
               $('#con2_id').val(ui.item.id);     
               $('#con2_gst').val(ui.item.gst);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#party_name").val('');
				$("#con2_id").val('');
				$("#con2_gst").val('');
				Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Consignee does not exists.</font>',});
                // alert('Consignee does not exist !'); 
              } 
              },
			}); 
});  	
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Add new unloading point : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
					<div class="row">		
						<form autocomplete="off" id="Form1">
						<div class="form-group col-md-12">&nbsp;</div>
						
						<div class="form-group col-md-4">
							<label>Destination/Unloading Location <sup><font color="red">*</font></sup></label>
							<input required="required" autocomplete="off" type="text" class="form-control" id="location" />
						</div>
						
						<input type="hidden" name="location" id="to_id">
						<input type="hidden" name="loc_lat_long" id="loc_lat_long">
						
						<div class="form-group col-md-3">
							<label>State <sup><font color="red">*</font></sup></label>
							<input autocomplete="off" type="text" id="state" readonly class="form-control">
						</div>
						
						<div class="form-group col-md-3">
							<label>Pincode <sup><font color="red">*</font></sup></label>
							<input autocomplete="off" type="text" id="pincode" readonly class="form-control">
						</div>
						
						<div class="form-group col-md-4">
							<label>Consignee <sup><font color="red">*</font></sup></label>
							<input required="required" autocomplete="off" type="text" class="form-control" id="party_name" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Consignee GST <sup><font color="red">*</font></sup></label>
							<input autocomplete="off" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" type="text" class="form-control" id="con2_gst" readonly />
						</div>
						
						<input type="hidden" name="party_id" id="con2_id">
						
						<div class="form-group col-md-3">
							<label>Way to Add ? <sup><font color="red">*</font></sup></label>
							<select class="form-control" id="way_to_add" required="required" name="way_to_add" onchange="WayTOAdd(this.value)">
								<option value="">--select an option--</option>
								<option value="Own_Truck">Own Truck</option>
								<option value="Google">Google Autocomplete</option>
								<option value="Coordinates">Coordinates</option>
							</select>
						</div>
						
						<script>
						function WayTOAdd(elem)
						{
							var to_id = $('#to_id').val();
							var party_id = $('#con2_id').val();
							
							if(to_id=='' || party_id=='')
							{ 
								Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter location and consignee first !</font>',});
								$('#way_to_add').val('');
							}
							else
							{
								$("#loadicon").show();
								jQuery.ajax({
									url: "./_check_loading_unloading_point.php",
									data: 'elem=' + elem + '&location=' + to_id + '&party_id=' + party_id + '&type=' + 'unloading',
									type: "POST",
									success: function(data) {
										$("#func_result").html(data);
									},
									error: function() {}
								});
							}
						}
						
						function VerifyPOI(stop_poi)
						{
							var loc_lat_long = $('#loc_lat_long').val();
							
							if(loc_lat_long=='')
							{
								Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Location not found !</font>',});
								$('#loading_point').val('');
							}
							else
							{
								if(stop_poi!='')
								{
									$("#loadicon").show();
									jQuery.ajax({
										url: "./_fetch_distance_bw_two_poi.php",
										data: 'stop_poi=' + stop_poi + '&loc_lat_long=' + loc_lat_long,
										type: "POST",
										success: function(data) {
											$("#func_result").html(data);
										},
										error: function() {}
									});
								}
							}
						}
						</script>
						
						<div class="form-group own_truck_div col-md-4">
							<label>OWN Truck Number <sup><font color="red">*</font></sup></label>
							<input id="own_tno" name="own_tno" autocomplete="off" required="required" oninput="this.value=this.value.replace(/[^A-Za-z0-9]/,'');" 
							type="text" class="form-control" />
						</div>
						
						<div class="form-group own_truck_div col-md-3">
							<label>Visit Date <sup><font color="red">*</font></sup></label>
							<input id="visit_date" name="visit_date" type="date" onchange="GetStops(this.value)" class="form-control" max="<?php echo date("Y-m-d"); ?>" required 
							pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
						</div>
						
						<div class="form-group own_truck_div col-md-3">
							<label>Select Unloading Point <sup><font color="red">*</font></sup></label>
							<select onchange="VerifyPOI(this.value)" name="loading_point" id="loading_point" class="form-control" required="required">
								<option value="">--select unloading point--</option>
							</select>
						</div>
						
						<div style="display:none" class="form-group google_div col-md-4">
							<label>Search Unloading Point - By google <sup><font color="red">*</font></sup></label>
							<input id="search_loading_point" name="search_loading_point" required="required" autocomplete="off" type="text" class="form-control" />
						</div>
						
						<div style="display:none" class="form-group google_div col-md-4">
							<label>Unloading Point Pincode <sup><font color="red">*</font></sup></label>
							<input id="loading_pincode" name="loading_pincode" readonly required="required" autocomplete="off" type="text" class="form-control" />
						</div>
						
						<div style="display:none" class="form-group cord_div col-md-3">
							<label>Latitude <sup><font color="red">*</font></sup></label>
							<input id="cord_lat" name="cord_lat" oninput="this.value=this.value.replace(/[^.0-9]/,'');" required="required" autocomplete="off" type="text" class="form-control" />
						</div>
						
						<div style="display:none" class="form-group cord_div col-md-3">
							<label>Longitude <sup><font color="red">*</font></sup></label>
							<input id="cord_long" name="cord_long" oninput="this.value=this.value.replace(/[^.0-9]/,'');" required="required" autocomplete="off" type="text" class="form-control" />
						</div>
						
						<input type="hidden" name="google_addr" id="google_addr">
						<input type="hidden" name="google_lat" id="google_lat">
						<input type="hidden" name="google_lng" id="google_lng">
						<input type="hidden" name="pincode_own_tno" id="pincode_own_tno">
						
						<input type="hidden" name="google_distance" id="google_distance">
						<input type="hidden" name="poi_type" value="unloading">
						
						<div class="form-group col-md-4">
							<label>Set Label for Unloading Point <sup><font color="red">*</font></sup></label>
							<input autocomplete="off" name="label" required oninput="this.value=this.value.replace(/[^A-Z a-z0-9,#.@/:;-]/,'');" type="text" class="form-control" id="label" />
						</div>
						
						<div class="form-group col-md-3">
							<label>Add for branch <sup><font color="red">*</font></sup></label>
							<select name="branch" class="form-control" required="required">
								<option value="">--select branch--</option>
								<?php
								$get_branch = Qry($conn,"SELECT username FROM user WHERE role='2' AND username NOT IN('HEAD','DUMMY') ORDER BY username ASC");
								
								while($row_branch = fetchArray($get_branch))
								{
									echo "<option value='$row_branch[username]'>$row_branch[username]</option>";
								}
								?>
							</select>
						</div>
						
						<div class="form-group col-md-3" id="man_pincode_div" style="display:none">
							<label>Loading Point Pincode <sup><font color="red">*</font></sup></label>
							<input id="pincode_manual" name="pincode_manual" oninput="this.value=this.value.replace(/[^0-9]/,'');" maxlength="6"  autocomplete="off" type="text" class="form-control" />
						</div>
						
						<div class="form-group col-md-3">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="submit" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="button_sub"><i id="submit_icon" class="fa fa-pencil-square-o" aria-hidden="true"></i> <i id="spinner_icon" style="display:none" class="fa fa-spinner fa-spin" aria-hidden="true"></i> &nbsp; Add Unloading Point</button>
						</div>
						</form>
				</div> 
				</div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>

<div id="func_result"></div>  

<script>
function GetStops(date)
{
	var tno = $('#own_tno').val();
	var location_id = $('#to_id').val();
	var party_id = $('#con2_id').val();
	
	if(tno!='')
	{
		if(location_id=='' || party_id=='')
		{
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Location or party not found !</font>',});
			$('#visit_date').val('');
		}
		else
		{
			$('#own_tno').attr('readonly',true);
			$('#location').attr('readonly',true);
			$('#party_name').attr('readonly',true);
			
			$("#loadicon").show();
			jQuery.ajax({
				url: "./_fetch_stops.php",
				data: 'veh_no=' + tno + '&date=' + date + '&poi_type=' + 'unloading' + '&location_id=' + location_id + '&party_id=' + party_id,
				type: "POST",
				success: function(data) {
					$("#loading_point").html(data);
				},
				error: function() {}
			});
		}
	}
	else
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Enter vehicle number first !</font>',});
		$('#visit_date').val('');
	}
}
</script>

<script>
function initMap() {
	
var options = {
  // types: ['(cities)'],
  componentRestrictions: {country: "in"}
 };
 
    var input = document.getElementById('search_loading_point');
  
    var autocomplete = new google.maps.places.Autocomplete(input, options);
   
    autocomplete.addListener('place_changed', function() {
		$('#button_sub').attr('disabled',true);
        var place = autocomplete.getPlace();
        var address = place.formatted_address;
        document.getElementById('google_addr').value = address;
        document.getElementById('google_lat').value = place.geometry.location.lat();
        document.getElementById('google_lng').value = place.geometry.location.lng();
		
		var addressComponent = place.address_components;
			
		for (var x = 0 ; x < addressComponent.length; x++) {
                var chk = addressComponent[x];
                if (chk.types[0] == 'postal_code') {
                    var zipCode = chk.long_name;
                }
            }
			
			 if (zipCode) {
                $('#loading_pincode').val(zipCode);
                $('#pincode1').val(zipCode);
				$('#button_sub').attr('disabled',false);
            }
            else {
                $('#loading_pincode').val('NA');
				$('#button_sub').attr('disabled',false);
            }
	});
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initMap&key=AIzaSyCZ6JUFGB8r5TpRYGnJjNxUH1NZdxaPoGw" async defer></script>