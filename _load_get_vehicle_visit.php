<?php
require_once("_connect.php");

$to_id = escapeString($conn,$_POST['to_id']);
$consignee = escapeString($conn,$_POST['consignee']);
$con2_id = escapeString($conn,$_POST['con2_id']);
$from_date = date('Y-m-d', strtotime($_POST['duration'], strtotime(date("Y-m-d"))));
$to_date = date("Y-m-d");

if($_POST['duration']=='FULL')
{
	if($consignee!='')
	{
		$qry = Qry($conn,"SELECT fstation,tstation,lrno,truck_no,branch,date,consignor,consignee 
		FROM lr_sample WHERE to_id='$to_id' AND con2_id='$con2_id' AND lr_type='OWN'");
	}
	else
	{
		$qry = Qry($conn,"SELECT fstation,tstation,lrno,truck_no,branch,date,consignor,consignee FROM lr_sample WHERE to_id='$to_id' AND lr_type='OWN'");
	}
}
else
{
	if($consignee!='')
	{
		$qry = Qry($conn,"SELECT fstation,tstation,lrno,truck_no,branch,date,consignor,consignee FROM lr_sample 
		WHERE date BETWEEN '$from_date' AND '$to_date' AND to_id='$to_id' AND con2_id='$con2_id' AND lr_type='OWN'");
	}
	else
	{
		$qry = Qry($conn,"SELECT fstation,tstation,lrno,truck_no,branch,date,consignor,consignee FROM lr_sample 
		WHERE date BETWEEN '$from_date' AND '$to_date' AND to_id='$to_id' AND lr_type='OWN'");
	}
}

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error While Processing Request.","../");
	exit();
}
?>
  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>From</th>
                        <th>To</th>
                        <th>LR_No</th>
                        <th>Vehicle_No</th>
                        <th>LR_Date</th>
                        <th>Branch</th>
                        <th>Consignor</th>
                        <th>Consignee</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='9'>No record found !</td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
			<td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			$lr_date = date("d-m-y",strtotime($row['date']));
			
			echo "<tr>
				<td>$i</td>
				<td>$row[fstation]</td>
				<td>$row[tstation]</td>
				<td>$row[lrno]</td>
				<td>$row[truck_no]</td>
				<td>$lr_date</td>
				<td>$row[branch]</td>
				<td>$row[consignor]</td>
				<td>$row[consignee]</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				  
<script>
$('#submit_icon').show();
$('#spinner').hide();
$('#add_btn').attr('disabled',false);
		
$(document).ready(function() {
    $('#example1').DataTable();
} );
</script>				  