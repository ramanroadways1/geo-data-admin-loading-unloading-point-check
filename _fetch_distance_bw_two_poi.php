<?php
require_once("./_connect.php");
 
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

echo "<script>$('#pincode_own_tno').val('');</script>";

$loading_loc_lat_long = escapeString($conn,($_POST['stop_poi']));
$from_loc_lat_long = escapeString($conn,($_POST['loc_lat_long']));

if(strlen($from_loc_lat_long)<4)
{
	AlertErrorTopRight("Location POI not found !");
	echo "<script>$('#loading_point').val('');</script>";
	exit();
}

if(strlen($loading_loc_lat_long)<4)
{
	AlertErrorTopRight("Loading point not found !");
	echo "<script>$('#loading_point').val('');</script>";
	exit();
}

// echo "<script>alert('$from_loc_lat_long')</script>";
// echo "<script>alert('$loading_loc_lat_long')</script>";

$pincode = explode(",",$loading_loc_lat_long)[2];

$origin = $from_loc_lat_long;
$destination = explode(",",$loading_loc_lat_long)[0].",".explode(",",$loading_loc_lat_long)[1];

$url="https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=".$google_api_key."";
$api = file_get_contents($url);
$data = json_decode($api);
			
$api_status = $data->rows[0]->elements[0]->status;
	
if($api_status=='NOT_FOUND')
{
	AlertErrorTopRight("Distance not found !");
	echo "<script>$('#loading_point').val('');</script>";
	exit();
}

if($api_status!='OK')
{
	AlertErrorTopRight("API Error !");
	echo "<script>$('#loading_point').val('');</script>";
	// echo "<span style='color:red;font-size:13px'>API Error: $api_status !</span>";
	exit();
}
			
$dest_addr = $data->destination_addresses[0];
$origin_addr = $data->origin_addresses[0];
$distance = round((int)$data->rows[0]->elements[0]->distance->value / 1000);
$travel_time = $data->rows[0]->elements[0]->duration->text;
$travel_time_value = $data->rows[0]->elements[0]->duration->value;
$travel_hrs = gmdate("H", $travel_time_value);
$travel_minutes = gmdate("i", $travel_time_value);
$travel_seconds = gmdate("s", $travel_time_value);

if($distance>80)
{
	AlertErrorTopRight("Distance between location and loading point is: $distance KMs !");
}

// if($distance>80)
// {
	// AlertErrorTopRight("Distance between location and loading point is: $distance KMs !");
	// echo "<script>$('#loading_point').val('');</script>";
	// exit();
// }
// else
// {
	if($pincode=='NA')
	{
		$get_pincode = getZipcode(explode(",",$loading_loc_lat_long)[0].",".explode(",",$loading_loc_lat_long)[1]);

		if(strlen($get_pincode)!=6)
		{
			// AlertErrorTopRight("Unable to fetch pincode !");
			// echo "<script>$('#loading_point').val('');</script>";
			// exit();
		}
		else
		{
			$pincode = $get_pincode;
		}
	}
// }

echo "<script>
	$('#pincode_own_tno').val('$pincode');
	$('#google_distance').val('$distance');
	$('#loadicon').fadeOut('slow');
</script>";
?>