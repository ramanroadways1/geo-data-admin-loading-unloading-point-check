<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$type = escapeString($conn,(trim($_POST['type'])));
$id = escapeString($conn,(trim($_POST['id'])));

AlertErrorTopRight("POI delete disabled !"); 
exit();
	
StartCommit($conn);
$flag = true;

if($type=='loading')
{
	$copy_data = Qry($conn,"INSERT INTO address_book_deleted(type,label,consignor_consignee,from_id_to_id,pincode,_lat,_long,google_km,
	approx_km,branch,branch_user,record_by,tno_visited,admin_update_timestamp,visit_date,timestamp_added,timestamp) SELECT '$type',label,
	consignor,from_id,pincode,_lat,_long,google_km,approx_km,branch,branch_user,record_by,tno_visited,admin_update_timestamp,visit_date,timestamp,
	'$timestamp' FROM address_book_consignor WHERE id='$id'");

	if(!$copy_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete = Qry($conn,"DELETE FROM address_book_consignor WHERE id='$id'");
	
	if(!$delete){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}
else
{
	$copy_data = Qry($conn,"INSERT INTO address_book_deleted(type,label,consignor_consignee,from_id_to_id,pincode,_lat,_long,google_km,
	approx_km,branch,branch_user,record_by,tno_visited,admin_update_timestamp,visit_date,timestamp_added,timestamp) SELECT '$type',label,
	consignee,to_id,pincode,_lat,_long,google_km,approx_km,branch,branch_user,record_by,tno_visited,admin_update_timestamp,visit_date,timestamp,
	'$timestamp' FROM address_book_consignee WHERE id='$id'");

	if(!$copy_data){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
	
	$delete = Qry($conn,"DELETE FROM address_book_consignee WHERE id='$id'");
	
	if(!$delete){
		$flag = false;
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#btn_edit_$id').attr('onclick','');
		$('#btn_delete_$id').attr('onclick','');
		
		$('#btn_edit_$id').attr('disabled',true);
		$('#btn_delete_$id').attr('disabled',true);
		
		$('#btn_edit_$id').html('Deleted');
		$('#btn_delete_$id').html('Deleted');
		
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>