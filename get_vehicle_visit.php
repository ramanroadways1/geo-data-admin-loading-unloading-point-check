<?php
include("_header.php");
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>  

<script>
$(function() {
		$("#location").autocomplete({
		source: '../b5aY6EZzK52NA8F/autofill/get_location.php',
		// appendTo: '#appenddiv',
		select: function (event, ui) { 
            $('#location').val(ui.item.value);   
            $('#to_id').val(ui.item.id);      
            return false;},
		change: function (event, ui) {
		if(!ui.item){
			$(event.target).val("");
            $(event.target).focus();
			$('#location').val("");   
			$('#to_id').val("");   
			Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Location does not exists.</font>',});
		}}, 
	focus: function (event, ui){
	return false;
	}
});});

$(function() {  
      $("#consignee").autocomplete({
			source: function(request, response) { 
			 $.ajax({
                  url: '../b5aY6EZzK52NA8F/autofill/get_consignee.php',
                  type: 'post',
                  dataType: "json",
                  data: {
                   search: request.term,
                  },
                  success: function( data ) {
                    response( data );
                  }
                 });
              },
              select: function (event, ui) { 
               $('#consignee').val(ui.item.value);   
               $('#con2_id').val(ui.item.id);     
               $('#con2_gst').html(ui.item.gst);     
			   return false;
              },
              change: function (event, ui) {  
                if(!ui.item){
                $(event.target).val(""); 
                $(event.target).focus();
				$("#consignee").val('');
				$("#con2_id").val('');
				$("#con2_gst").html('');
                Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Consignee does not exists.</font>',});
              } 
              },
			}); 
});
</script>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;">Check Vehicle Visit : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
				<div class="col-md-12">
					<div class="row">		
				<form autocomplete="off" id="Form1">		
						<div class="form-group col-md-12">&nbsp;</div>
						
						<div class="form-group col-md-3">
							<label>Destination Location <sup><font color="red">*</font></sup></label>
							<input required="required" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'');" type="text" class="form-control" id="location" />
						</div>
						
						<input type="hidden" name="to_id" id="to_id">
						<input type="hidden" name="con2_id" id="con2_id">
						
						<div class="form-group col-md-3">
							<label>Date Range <sup><font color="red">*</font></sup></label>
							<select style="font-size:12px" name="duration" id="duration" class="form-control" required>
								<option style="font-size:12px" value="">---select date range---</option>
								<option style="font-size:12px" value="-0 days">Today's</option>
								<option style="font-size:12px" value="-1 days">Last 2 days</option>
								<option style="font-size:12px" value="-4 days">Last 5 days</option>
								<option style="font-size:12px" value="-6 days">Last 7 days</option>
								<option style="font-size:12px" value="-9 days">Last 10 days</option>
								<option style="font-size:12px" value="-14 days">Last 15 days</option>
								<option style="font-size:12px" value="-29 days">Last 30 days</option>
								<option style="font-size:12px" value="-59 days">Last 60 days</option>
								<option style="font-size:12px" value="-89 days">Last 90 days</option>
								<option style="font-size:12px" value="-119 days">Last 120 days</option>
								<option style="font-size:12px" value="FULL">FULL REPORT</option>
							</select>
						</div>
						
						<div class="form-group col-md-4">
							<label>Consignee & GST: <span style="color:blue;letter-spacing:.5px" id="con2_gst"></span></label>
							<input required="required" autocomplete="off" oninput="this.value=this.value.replace(/[^A-Z a-z0-9]/,'');" type="text" class="form-control" id="consignee" />
						</div>
						
						<div class="form-group col-md-2">
							<?php if(!isMobile()) { echo "<label>&nbsp;</label><br />"; } ?>
							<button type="button" onclick="Search()" class="btn btn-sm btn-success <?php if(isMobile()) { echo "btn-block"; } ?>" id="add_btn">
							<i id="submit_icon" class="fa fa-search" aria-hidden="true"></i> <i id="spinner_icon" style="display:none"
							class="fa fa-spinner fa-spin" aria-hidden="true"></i> &nbsp; Search </button>
						</div>
				</form>		
				</div> 
				</div> 
				<div class="col-md-12 table-responsive" id="table_load">
				<table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>From</th>
                        <th>To</th>
                        <th>LR_No</th>
                        <th>Vehicle_No</th>
                        <th>LR_Date</th>
                        <th>Branch</th>
                        <th>Consignor</th>
                        <th>Consignee</th>
                      </tr>
                    </thead>
                    <tbody>
			<tr>
				<td colspan='9'>No record found !</td>
				<td style='display: none'></td>
				<td style='display: none'></td>
				<td style='display: none'></td>
				<td style='display: none'></td>
				<td style='display: none'></td>
				<td style='display: none'></td>
				<td style='display: none'></td>
				<td style='display: none'></td>
				<td style='display: none'></td>
              </tr>
			  </tbody>
                    </table>
				</div>
                </div><!-- /.box-body --> 
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php include("_footer.php") ?>

<div id="func_result"></div>  

<script>
function Search()
{
	var to_id = $('#to_id').val();
	var con2_id = $('#con2_id').val();
	var consignee = $('#consignee').val();
	var duration = $('#duration').val();
	
	if(to_id=='' || duration=='')
	{
		Swal.fire({icon: 'warning',html: '<font size=\'2\' color=\'black\'>Select destination and visit-date range !</font>',});
		$('#visit_date').val('');
	}
	else
	{
		$('#submit_icon').hide();
		$('#spinner').show();
		$('#add_btn').attr('disabled',true);
		jQuery.ajax({
				url: "./_load_get_vehicle_visit.php",
				data: 'to_id=' + to_id + '&duration=' + duration + '&con2_id=' + con2_id + '&consignee=' + consignee,
				type: "POST",
				success: function(data) {
					// $("#table_load").html(data);
					$("#table_load").html(data);
			   // $('#example1').DataTable({ 
                 // "destroy": true, //use for reinitialize datatable
              // });
				},
				error: function() {}
			});
	}
}

// $(document).ready(function() {
    // $('#example1').DataTable();
// } );
</script>