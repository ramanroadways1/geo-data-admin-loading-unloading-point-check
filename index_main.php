<?php
include("_header.php");

$get_con1 = Qry($conn,"SELECT id FROM address_book_consignor");

$total_con1 = numRows($get_con1);

$get_con2 = Qry($conn,"SELECT id FROM address_book_consignee");

$total_con2 = numRows($get_con2);

$get_con1_today = Qry($conn,"SELECT id FROM address_book_consignor WHERE date(timestamp)='".date("Y-m-d")."'");

$total_con1_today = numRows($get_con1_today);

$get_con2_today = Qry($conn,"SELECT id FROM address_book_consignee WHERE date(timestamp)='".date("Y-m-d")."'");

$total_con2_today = numRows($get_con2_today);
?>

<div class="content-wrapper">
       <section class="content-header">
          <h1 style="font-size:16px;">Dashboard</h1>
       </section>

		<section class="content">
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $total_con1_today; ?></h3>
                  <p>Loading Point Added today</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
            
			<div class="col-lg-3 col-xs-6">
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $total_con2_today; ?></h3>
                  <p>Unloading Point Added today</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
			
            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-yellow">
                <div class="inner">
                 <h3><?php echo $total_con1; ?></h3>
                  <p>Total Loading Points</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
			
            <div class="col-lg-3 col-xs-6">
              <div class="small-box bg-red">
                <div class="inner">
                 <h3><?php echo $total_con2; ?></h3>
                   <p>Total Unloading Points</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
              </div>
            </div>
          </div>
        </section><!-- /.content -->
      </div>
	  <!-- /.content-wrapper -->
     
<?php include("_footer.php"); ?>
