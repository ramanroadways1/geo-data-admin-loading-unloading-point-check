<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$party_type = escapeString($conn,$_POST['party_type']);
$label = escapeString($conn,strtoupper(trim($_POST['label'])));
$id = escapeString($conn,(trim($_POST['id'])));
$_lat = escapeString($conn,(trim($_POST['lat'])));
$_long = escapeString($conn,(trim($_POST['long'])));
$pincode = escapeString($conn,(trim($_POST['pincode'])));

if($party_type=='consignor')
{
	$table_name="address_book_consignor";
}
else if($party_type=='consignee')
{
	$table_name="address_book_consignee";
}
else
{
	AlertErrorTopRight("Invalid party type !");
	echo "<script>$('#update_button').attr('disabled',false);</script>";
	exit();
}

if(strlen($pincode)!=6)
{
	AlertErrorTopRight("Invalid pincode !");
	echo "<script>$('#update_button').attr('disabled',false);</script>";
	exit();
}

$select_data = Qry($conn,"SELECT label,pincode,_lat,_long FROM `$table_name` WHERE id='$id'");

if(!$select_data){
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#update_button').attr('disabled',false);</script>";
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($select_data)==0)
{
	AlertErrorTopRight("No record found !");
	echo "<script>$('#update_button').attr('disabled',false);</script>";
	exit();
}

$row = fetchArray($select_data);

$update_log = array();
$update_Qry = array();

$update_table = "NO";

if($label!=$row['label'])
{
	$update_log[]="Label : $row[label] to $label";
	$update_Qry[]="label='$label'";
}

if($_lat!=$row['_lat'])
{
	$update_log[]="Lat : $row[_lat] to $_lat";
	$update_Qry[]="_lat='$_lat'";
	$update_table = "YES";
}

if($_long!=$row['_long'])
{
	$update_log[]="Long : $row[_long] to $_long";
	$update_Qry[]="_long='$_long'";
	$update_table = "YES";
}

if($pincode!=$row['pincode'])
{
	$update_log[]="Pincode : $row[pincode] to $pincode";
	$update_Qry[]="pincode='$pincode'";
	$update_table = "YES";
}

$update_log = implode(', ',$update_log); 
$update_Qry = implode(', ',$update_Qry); 

$lat_long = $_lat.",".$_long;

if($update_log=="")
{
	AlertErrorTopRight("Nothing to update !");
	echo "<script>$('#update_button').attr('disabled',false);</script>";
	exit();
}

StartCommit($conn);
$flag = true;

$update = Qry($conn,"UPDATE `$table_name` SET $update_Qry WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$insertLog = Qry($conn,"INSERT INTO edit_log_admin(table_id,vou_no,vou_type,section,edit_desc,branch,edit_by,timestamp) VALUES 
('$id','$id','Loading_Unloading_Update','$party_type','$update_log','','ADMIN','$timestamp')");

if(!$insertLog){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($update_table=='YES')
{
	if($party_type=='consignor')
	{
		$update_trip = Qry($conn,"UPDATE dairy.trip SET from_poi='$lat_long',from_pincode='$pincode' WHERE addr_book_id_consignor='$id'");

		if(!$update_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_trip_final = Qry($conn,"UPDATE dairy.trip_final SET from_poi='$lat_long',from_pincode='$pincode' WHERE addr_book_id_consignor='$id'");

		if(!$update_trip_final){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
	else
	{
		$update_trip = Qry($conn,"UPDATE dairy.trip SET to_poi='$lat_long',to_pincode='$pincode',pincode='$pincode' 
		WHERE addr_book_id_consignee='$id'");

		if(!$update_trip){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
		
		$update_trip_final = Qry($conn,"UPDATE dairy.trip_final SET to_poi='$lat_long',to_pincode='$pincode',pincode='$pincode' 
		WHERE addr_book_id_consignee='$id'");

		if(!$update_trip_final){
			$flag = false;
			errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		}
	}
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#update_button').attr('disabled',false);
		$('#label_col_$id').html('$label');
		$('#coordinates_row_$id').attr('href','https://www.google.com/maps/place/$lat_long');
		$('#pincode_row_$id').html('$pincode');
		$('#close_modal_btn')[0].click();
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	echo "<script>$('#update_button').attr('disabled',false);</script>";
	exit();
}	
?>