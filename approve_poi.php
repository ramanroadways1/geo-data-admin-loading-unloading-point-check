<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,(trim($_POST['id'])));
$type = escapeString($conn,(trim($_POST['type'])));

StartCommit($conn);
$flag = true;

if($type=='loading')
{
	$table_name="address_book_consignor";
}
else
{
	$table_name="address_book_consignee";
}

$update = Qry($conn,"UPDATE `$table_name` SET admin_update_timestamp='$timestamp' WHERE id='$id'");

if(!$update){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	echo "<script>
		$('#btn_edit_$id').attr('disabled',true);
		$('#btn_delete_$id').attr('disabled',true);
		$('#btn_approve_$id').attr('disabled',true);
		
		$('#btn_edit_$id').html('Approved');
		$('#btn_delete_$id').html('Approved');
		$('#btn_approve_$id').html('Approved');
		
		$('#btn_edit_$id').attr('onclick','');
		$('#btn_delete_$id').attr('onclick','');
		$('#btn_approve_$id').attr('onclick','');
	</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>