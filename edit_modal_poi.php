<?php
require_once '_connect.php';

$date = date("Y-m-d");
$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,$_POST['id']);

	$qry_get_data = Qry($conn,"SELECT a.label,a._lat,a._long,a.pincode,a.record_by,a.tno_visited,a.visit_date,c.name as party_name,l.name as location 
	FROM address_book_consignor AS a
	LEFT OUTER JOIN consignor AS c ON c.id = a.consignor 
	LEFT OUTER JOIN station AS l ON l.id = a.from_id 
	WHERE a.id = '$id'");
	
	if(!$qry_get_data){
		AlertErrorTopRight("Error while processing request !");
		errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
		exit();
	}

$row22 = fetchArray($qry_get_data);
?>
<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#Modal1"></button>

<form id="EditPoiForm" autocomplete="off">
<div class="modal" id="Modal1" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header bg-primary">
		<h4 style="font-size:15px;" class="modal-title">Edit Loading Point :</h4>
      </div>
	 <div class="modal-body">
	  <div id="tab_result"></div>
		 <div class="row">
			
			<div class="form-group col-md-12">
				<label>Loading Point Label <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="text" value="<?php echo $row22['label']; ?>" oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" class="form-control" name="label" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Loading Location <sup><font color="red">*</font></sup></label>
				<textarea style="font-size:12px" readonly id="location" class="form-control" name="location" required><?php echo $row22['location']; ?></textarea>
			</div>
			
			<div class="form-group col-md-6">
				<label>Consignor <sup><font color="red">*</font></sup></label>
				<textarea style="font-size:12px" readonly class="form-control" name="party" required><?php echo $row22['party_name']; ?></textarea>
			</div>
			
			<div class="form-group col-md-6">
				<label>Latitude <font color="red">*</font></label>
				<input type="text" value="<?php echo $row22['_lat']; ?>" oninput="this.value=this.value.replace(/[^0-9.]/,'')" class="form-control" name="lat" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Longitude <font color="red">*</font></label>
				<input type="text" value="<?php echo $row22['_long']; ?>" oninput="this.value=this.value.replace(/[^0-9.]/,'')" class="form-control" name="long" required />
			</div>
			
			<div class="form-group col-md-6">
				<label>Pincode <font color="red">*</font></label>
				<input type="text" value="<?php echo $row22['pincode']; ?>" maxlength="6" oninput="this.value=this.value.replace(/[^0-9]/,'')" class="form-control" name="pincode" required />
			</div>
			
		</div>
			<input type="hidden" name="party_type" value="consignor">	
			<input type="hidden" name="id" value="<?php echo $id; ?>">	
	  </div>

      <div class="modal-footer">
        <button type="submit" id="update_button" class="btn btn-sm btn-primary">Update</button>
        <button type="button" id="close_modal_btn" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script type="text/javascript">
$(document).ready(function (e) {
$("#EditPoiForm").on('submit',(function(e) {
$("#loadicon").show();
$("#update_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_edit_poi.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<script>
	$('#ModalButton')[0].click();
	$('#loadicon').fadeOut('slow');
</script>