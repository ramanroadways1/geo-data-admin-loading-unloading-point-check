<?php
include("_header_datatable.php");
?>

<div class="content-wrapper">
      <section class="content-header">
          <h1 style="font-size:16px;"><font color="green"><b>Approved</font></b> Loading Points : </h1>
       </section>
       
	   <section class="content">
          <div class="row">
            <div class="col-xs-12">
			<div class="box">
                <div class="box-body">
<?php
$qry = Qry($conn,"SELECT a.id,a.code,a.label,a._lat,a._long,a.pincode,a.google_km,a.branch,a.record_by,a.tno_visited,a.visit_date,a.timestamp,s.name as location,
u.name as username,p.name as party,p.gst,a.admin_update_timestamp 
FROM address_book_consignor AS a 
LEFT OUTER JOIN station AS s ON s.id = a.from_id 
LEFT OUTER JOIN consignor AS p ON p.id = a.consignor 
LEFT OUTER JOIN emp_attendance AS u ON u.code = a.branch_user 
WHERE a.admin_update_timestamp IS NOT NULL ORDER BY a.id ASC");

if(!$qry){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}
?>			  
	
				<div class="col-md-12 table-responsive" id="load_table_div">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#Code</th>
                        <th>Label</th>
                        <th>Location & Consignor</th>
						<th>Coordinates</th>
						<th>Pincode</th>
                        <th>Distance</th>
                        <th>Branch & User</th>
                        <th>Record Details</th>
                        <th>Timestamp</th>
                        <th>Approve Timestamp</th>
                      </tr>
                    </thead>
                    <tbody>
	<?php
	if(numRows($qry)==0)
	{
		echo "<tr>
			<td colspan='10'>No record found !</td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
			 <td style='display: none'></td>
		</tr>";
	}
	else
	{
		$i=1;
		while($row = fetchArray($qry))
		{
			$timestamp = date("d-m-y h:i A",strtotime($row['timestamp']));
			$admin_update_timestamp = date("d-m-y h:i A",strtotime($row['admin_update_timestamp']));
			
			if($row['tno_visited']!=''){
				$visit_date = "<br>(".date("d-m-y",strtotime($row['visit_date'])).")";
			}else{
				$visit_date="";
			}
			echo "<tr>
				<td>$row[code]</td>
				<input type='hidden' id='label_html_$row[id]' value='$row[label]'>
				<td id='label_col_$row[id]'>$row[label]<br><button type='button' onclick='EditLabel($row[id])' class='btn btn-xs btn-default'><li class='fa fa-pencil-square-o'> Edit</button></td>
				<td>$row[location]-><br>$row[party]<br>($row[gst])</td>
				<td><a id='coordinates_row_$row[id]' href='https://www.google.com/maps/place/$row[_lat],$row[_long]' target='_blank'><button class='btn btn-xs btn-warning'>View GMap</button></a></td>
				<td id='pincode_row_$row[id]'>$row[pincode]</td>
				<td>$row[google_km]</td>
				<td>$row[branch]<br>($row[username])</td>
				<td>$row[record_by]<br>$row[tno_visited]$visit_date</td>
				<td>$timestamp</td>
				<td>$admin_update_timestamp</td>
			</tr>";
		$i++;	
		}
	}
	?>	
                    </tbody>
                  </table>
				 </div> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script>
function EditLabel(id)
{
	var label_name = $('#label_html_'+id).val();
	$('#label_name1').val(label_name);
	$('#modal_id').val(id);
	$('#ModalButton')[0].click();
}
</script>

<button type="button" id="ModalButton" style="display:none" class="btn btn-primary" data-toggle="modal" data-target="#Modal1"></button>

<form id="EditPoiForm" autocomplete="off">
<div class="modal" id="Modal1" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header bg-primary">
		<h4 style="font-size:15px;" class="modal-title">Edit Label :</h4>
      </div>
	 <div class="modal-body">
	  <div id="tab_result"></div>
		 <div class="row">
			
			<div class="form-group col-md-12">
				<label>Label Name <sup><font color="red">*</font></sup></label>
				<input style="font-size:12px" type="text" id="label_name1" oninput="this.value=this.value.replace(/[^a-z A-Z0-9./-]/,'')" class="form-control" name="label" required />
			</div>
		
		</div>
			<input type="hidden" name="party_type" value="consignor">	
			<input type="hidden" id="modal_id" name="id">	
	  </div>

      <div class="modal-footer">
        <button type="submit" id="update_button" class="btn btn-sm btn-primary">Update</button>
        <button type="button" id="close_modal_btn" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
      </div>
	</form>
    </div>
  </div>
</div>	

<script type="text/javascript">
$(document).ready(function (e) {
$("#EditPoiForm").on('submit',(function(e) {
$("#loadicon").show();
$("#update_button").attr("disabled", true);
e.preventDefault();
	$.ajax({
	url: "./save_edit_label.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
		$("#func_result").html(data);
	},
	error: function() 
	{} });}));});
</script>

<?php include("_footer_datatable.php") ?>

<div id="func_result"></div>  
<div id="func_result12"></div>  